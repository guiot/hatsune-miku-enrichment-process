Factorio mod that replaces the strangely-named 'Kovarex enrichment process' with 'Hatsune Miku enrichment process', in the honor of Hatsune Miku, Factorio's creator.

The mod is currently localized for English and Italian only. Feel free to submit a translation as a pull request.
